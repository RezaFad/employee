import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { config } from './orm.config'
import { EmployeeModule } from './employee/employee.module';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ProductModule } from './product/product.module';

@Module({
  imports: [TypeOrmModule.forRoot(config), EmployeeModule, AuthModule, UsersModule, ProductModule],
  exports: [],
  providers: [AppService],
})
export class AppModule {}
