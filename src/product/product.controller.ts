import { Controller, UseGuards, Get, Param, Post, Body, ValidationPipe } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Product } from "./product.entity";
import { Repository } from "typeorm";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";
import { ProductDto } from "./product.dto";
import { ProductsService } from "./product.service";

@Controller('product')
export class ProductController {
    constructor(@InjectRepository(Product) private productRepository: Repository<Product>, private productService: ProductsService) { }
    
    @UseGuards(JwtAuthGuard)
    @Get(':id?')
    async getData(@Param('id') id?: any): Promise<Product | Product[]> {
        if (id) {
            return await this.productRepository.findOne({
                where: {
                    id: id
                }
            })
        } else {
            return await this.productRepository.find()
        }
    }

    @UseGuards(JwtAuthGuard)
    @Post(':id?')
    async post(@Body(ValidationPipe) data: ProductDto, @Param('id') id?: any): Promise<any> {
        let item: Product = null

        if (id) {
            item = await this.productRepository.findOne({
                where: {
                    id: id
                }
            })
        } else {
            item = new Product()
        }

        item.name = data.name
        item.company = data.company
        let a = await this.productService.findUsersById(data.users)
        item.users = a

        return await this.productService.create(item)
    }
}