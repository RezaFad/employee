import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Product } from "./product.entity";
import { UsersService } from "src/users/users.service";
import { Users } from "src/users/users.entity";



@Injectable()
export class ProductsService {
    constructor(@InjectRepository(Product) private productRepository: Repository<Product>,
    private readonly userService: UsersService) { }

    async findById(id: number):Promise<Product | undefined> {
        return await this.productRepository.findOne({
            where: {
                id: id
            }
        })
    }

    async findUsersById(id: number): Promise<Users | undefined> {
        return await this.userService.findById(id)
    }

    async create(product: Product): Promise<any> {
        return await this.productRepository.save(product)
    }
}