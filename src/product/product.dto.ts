import { IsString, IsNumber, Min, Max, IsNumberString, IsInt } from 'class-validator'
import { Type } from 'class-transformer';
 
export class ProductDto {
    @IsString()
    name

    @IsString()
    company

    @Type(() => Number)
    @IsInt()
    users
}