import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, OneToMany } from "typeorm";
import * as crypto from 'crypto'
import { Product } from "src/product/product.entity";

@Entity()
export class Users {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    username: string

    @BeforeInsert()
    hashPassword() {
        this.password = crypto.createHmac('sha256', this.password).digest('hex');
    }

    @Column()
    password: string

    @Column()
    name: string

    @OneToMany(type => Product, product => product.users)
    product: Product[]

}