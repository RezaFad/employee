import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from './users.entity';
import { Repository } from 'typeorm';

export type User = any

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(Users) private userRepository: Repository<Users>) { }


    async findOne(username: string): Promise<Users | undefined> {
        return await this.userRepository.findOne({ 
            where: {
                username: username
            }
        })
    }

    async findById(id: number): Promise<Users | undefined> {
        return await this.userRepository.findOne({
            where: {
                id: id
            }
        })
    }

    async create(user: Users): Promise<Users> {
        return await this.userRepository.save(user)
    }
}
