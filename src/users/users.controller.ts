import { Controller, Post, Body, ValidationPipe, Param, UseGuards, Get } from "@nestjs/common";
import { UserDto } from "./users.dto";
import { Users } from "./users.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { UsersService } from "./users.service";
import { JwtAuthGuard } from "src/auth/jwt-auth.guard";

@Controller('users')
export class UserController {
    constructor(@InjectRepository(Users) private userRepository: Repository<Users>, private readonly userService: UsersService) { }

    @UseGuards(JwtAuthGuard)
    @Get(':id?')
    async getData(@Param('id') id?: any): Promise<Users[] | Users> {
        if(id) {
            return await this.userService.findById(id)
        } else {
            return await this.userRepository.find()
        }
    }
    
    @UseGuards(JwtAuthGuard)
    @Post(':id?')
    async post(@Body(ValidationPipe) data: UserDto, @Param('id') id?: any): Promise<any> {
        let user: Users = null;
        
        if(id) {
            user = await this.userService.findById(id)
        } else {
            user = new Users()
        }

        user.name = data.name
        user.username = data.username
        user.password = data.password

        return await this.userService.create(user)
    }
}