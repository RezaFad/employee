import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity()
export class Employee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstname: string;

    @Column()
    lastname: string;
    
    @Column()
    age: number;

    @Column()
    job: string;
}