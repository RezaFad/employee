import { IsString, IsInt } from 'class-validator'
import { Transform } from 'class-transformer'

export class EmployeeDto {
    @IsString()
    firstname: string;

    @IsString()
    lastname: string;

    @Transform(value => Number.isNaN(+value) ? 0 : +value)
    @IsInt()
    age: number

    @IsString()
    job: string
}