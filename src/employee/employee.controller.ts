import { Controller, Get, NotFoundException, Post, Body, ValidationPipe, Param, Delete, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from './employee.entity';
import { Repository } from 'typeorm';
import { EmployeeDto } from './employee.dto';

@Controller('employee')
export class EmployeeController {
    constructor(@InjectRepository(Employee) private repository: Repository<Employee>) { }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get()
    async getAll(): Promise<Employee[]> {
        return await this.repository.find()
    }

    @Get(':id?')
    async getEmployee(@Param('id') id?: any): Promise<Employee>
    {
        if (id) {
            return await this.getUser(id)
        }
    }

    @Post(':id?')
    async post(@Body(ValidationPipe) data: EmployeeDto, @Param('id') id?: any): Promise<any>
    {
        let newEmployee: Employee = null;
        if(id) {
            newEmployee = await this.getUser(id)
        } else {
            newEmployee = new Employee()
        }

        newEmployee.firstname = data.firstname
        newEmployee.lastname = data.lastname
        newEmployee.age = data.age
        newEmployee.job = data.job
        await this.repository.save(newEmployee)
        return true
    }

    @Delete(':id?')
    async delete(@Param('id') id?: number): Promise<any>
    {
        const employee = await this.getUser(id)
        return await this.repository.remove(employee)
    }

    async getUser(id): Promise<Employee> {
        const employee = await this.repository.findOne(id)
        if(!employee) throw new NotFoundException("Data Not Found")
        return employee
    }
}
