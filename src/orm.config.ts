import { TypeOrmModuleOptions } from '@nestjs/typeorm'
export const config: TypeOrmModuleOptions = {
    type: 'postgres',
    username: 'postgres',
    password: 'postgres',
    port: 5432,
    host: 'localhost',
    database: 'employee',
    synchronize: true,
    entities: ['dist/**/*.entity{.ts,.js}']
}