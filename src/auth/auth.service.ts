import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as crypto from 'crypto'
import { JwtService } from '@nestjs/jwt'

@Injectable()
export class AuthService {
    constructor(private userService: UsersService, private jwtService: JwtService) { }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.userService.findOne(username);
        const cryp = crypto.createHmac('sha256', pass).digest('hex');

        if (user && user.password === cryp) {
            const { password, ...result } = user;
            return result
        }

        return null
    }

    async login(user: any) {
        const payload = { username: user.username, sub: user.userId };
        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}
